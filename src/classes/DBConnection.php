<?php
namespace DealerInspire\Database;

/**
 * DataBase is a class for creating a database connection for dependency injection
 *
 * @package    Database
 * @category   Data
 * @author     Author <kaozotto@gmail.com>
 * @copyright  2019 Dealer Inspire
 * @version    1.0
 */
class DBConnection{

    private $pdo, $dbname;

    /**
     * constructor, create an instance of the class
     *
     * @return Object 
     * @access public
     */
    public function __construct() {

        // database config
        require '../../db/db_config.php';

        $this->dbname = $dbname;

        try {
            $this->pdo = new \PDO("mysql:host=$host", $username, $password, $options);
        }
        catch(PDOException $error) {
            die( "We are having trouble processing your information. Please contact Customer Service at 1-800-cst-serv: " . Exception($error->getMessage()) );
        }
    }


    /**
     * Save information to the database
     *
     * @param String $sql the sql string to execute
     * @param Array $values the replacement $values
     * @access public
     */
    public function writeToDatabase( $sql, $values ){

        // replace sql string with db name
        $sql = str_replace("%dbname%", $this->dbname, $sql);

        // save information
        try {
            $query = $this->pdo->prepare($sql);
            $query->execute($values);
        }
        catch(PDOException $error) {
            die( "We are having trouble processing your information. Please contact Customer Service at 1-800-cst-serve: " . Exception($error->getMessage()) );
        }
        $query = null;
    }
}
