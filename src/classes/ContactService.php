<?php
namespace DealerInspire\Email;

/**
 * ContactService is a class for sending contact emails
 *
 * @package    Communication
 * @category   Email
 * @author     Author <kaozotto@gmail.com>
 * @copyright  2019 Dealer Inspire
 * @version    1.0
 */
class ContactService{

    public $template;

    /**
     * constructor, create an instance of the class
     *
     * @param String $template the email template to use
     *
     * @return Object 
     * @access public
     */
    public function __construct( $template ) {
        $this->template = $template;
    }


    /**
     * Validate required params for contact email
     *
     * @param Array $post the message params
     *
     * @return int
     *
     * @access public
     */
    public static function validate( $post ){

        // required parameters
        if( empty( $post['message'] ) || empty( $post['full_name'] ) || empty( $post['email'] ) ){
            return 0;
        }

        // bad email address
        if( filter_var($post['email'], FILTER_VALIDATE_EMAIL) ){
            return 1;
        }
        else{
             return 0;
        }
    }

    /**
     * Send the email
     *
     * @param String $to the recipeinet email address
     * @param String $from the from email address
     * @param String $fromName the senders full name
     * @param String $message the email message
     * @param String $telephone the telephone number
     *
     * @access public
     */
    public function sendMail( $to, $from, $fromName, $message, $telephone){

        $subject = "Customer Contact";
        $html = file_get_contents( dirname(__FILE__) . "/../email-templates/" . $this->template );

        $html = str_replace(
            ["%name%", "%email%", "%message%", "%telephone%"],
            [$fromName, $from, $message, $telephone],
            $html
        );

        // Set content-type header for sending HTML email 
        $headers = "MIME-Version: 1.0" . "\r\n"; 
        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n"; 
         
        // Send email 
        if(mail($to, $subject, $html, $headers)){ 
            echo 'Thank you for your inquiry. We will get back to you soon!'; 
        }else{ 
           echo 'Email sending failed! Please contact Customer Service at 1-800-cst-serv'; 
        }
    }
}
