CREATE DATABASE dealer_inspire_db;

use dealer_inspire_db;

CREATE TABLE contacts (
	id INT(11) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
	full_name VARCHAR(255) NOT NULL, 
	email VARCHAR(255) NOT NULL, 
	telephone VARCHAR(255) NOT NULL, 
	message TEXT NOT NULL
) engine=MyISAM;
