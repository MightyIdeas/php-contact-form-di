<?php

/**
 * Configuration for database connection
 *
 * IMPORTANT!: if you change $dbname, please also change it on line 1 and line 3 of db/init.sql
 * before running db/db_install.php
 *
 */

$host       = "localhost"; // local host
$username   = "root"; // database username for local host
$password   = "root99"; // user password
$dbname     = "dealer_inspire_db"; // database name
$dsn        = "mysql:host=$host;dbname=$dbname";
$options    = [
                PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
              ];
