/**
 *
 *	Javascript controller for the Contact Form
 *
 */

jQuery(document).ready(function($) {

    "use strict";

    // the contact form
    const contactForm = $("#widget-contact-form");

    // the ajax endpoint for the contact form
    const endPoint = "./ajax/widget-contact-form-ajax.php";

    // the form rules for validation
    const formRules = {
        rules: {
            full_name: "required",
            email: {
                required: true,
                email: true
            },
            message: {
                required: true
            }
        },
        messages: {
            full_name: "Please enter your full name",
            email: "Please enter a valid email address",
            message: "Please enter your message"
        },
        submitHandler: function() {

			let data = {};
			let formData = contactForm.serializeArray();

			for (var i = 0; i < formData.length; i++){
				data[formData[i]['name']] = formData[i]['value'];
			}

			$.ajax({
				method: 'POST',
			    data: data,
			    url: endPoint
			}).done(function(data) {
			    // If successful
			   $('#contact-result').html( data );
			}).fail(function(jqXHR, textStatus, errorThrown) {
			    // If fail
			    $('#contact-result').html( "There was a problem handling your request: " + textStatus + ': ' + errorThrown );
			});

        	// $('#contact-result').load(endPoint, {values: data});
        }
    };

    /**
     *	Main Init Function
     */
    (function init() {

    	contactForm.validate( formRules );

    })();

}); // EOF document.ready
