<?php
/**
 * Ajax handler for contact form
 *
 */

namespace DealerInspire\Contact;

// contact service
require (dirname(__FILE__) . '/../../src/classes/ContactService.php');
use DealerInspire\Email\ContactService;

// database
require (dirname(__FILE__) . '/../../src/classes/DBConnection.php');
use DealerInspire\Database\DBConnection;

// validate required information from the post
$valid = ContactService::validate($_POST);
if( empty($valid) ){
	die("Contact requirements missing. Your message was not transmitted!" );
}

$full_name = $_POST['full_name'];
$email = $_POST['email'];
$telephone = empty($_POST['telephone']) ? "" : $_POST['telephone'];
$message = $_POST['message'];

// create DB Connection
$db = new DBConnection();

// create query to store contact message and information
$sql = "
    INSERT INTO
        %dbname%.contacts (full_name, email, telephone, message )
    VALUES (?, ?, ?, ?)
";

// db values
$db_values = [
	$full_name,
	$email,
	$telephone,
	$message
];

// save to database
$db->writeToDatabase( $sql, $db_values );

// send contact email
$mailer = new ContactService( "contact.html" );
$mailer->sendMail( "guy-smiley@example.com", $email, $full_name, $message, $telephone );
