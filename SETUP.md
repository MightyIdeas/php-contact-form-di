# Setup

Instructions for setting up and testing

## Database

Install the database for storing contact information:

1) edit php-contact-form-di/db/ with your DB credentials  
* IMPORTANT!: if you change $dbname, please also change it on line 1 and line 3 of db/init.sql  
2) run the db install script:  

```
cd to your checkout of php-contact-form-di
run: php db/db_install.php
```

## Unit testing

My tests are organized into the tests folder.

```
cd tests
run: phpunit ./
```  
Note: I have phpunit installed globally. If you do not have it installed globaly ou will have to run:  
php [path to your phpunit]/phpunit ./
