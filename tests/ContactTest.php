<?php
require '../src/classes/ContactService.php';
use DealerInspire\Email\ContactService;

use PHPUnit\Framework\TestCase;

class ContactTest extends TestCase{

    private $contactTest;

    protected function setUp(){
        $this->contactTest = new ContactService( "contact.html" );
    }

    protected function tearDown(){
        $this->contactTest = NULL;
    }

    public function testValidContactData(){

        // valid contact information
        $result = $this->contactTest->validate(
            [
                "message" => "hello",
                "full_name" => "Frank Doe",
                "email" => "frank@doe.com"
            ]
        );
        $this->assertEquals(1, $result);

        // invalid contact information
        $result = $this->contactTest->validate(
            [
                "message" => "hello",
                "full_name" => "Joe Shmoe"
            ]
        );
        $this->assertEquals(0, $result);

        // invalid email address
        $result = $this->contactTest->validate(
            [
                "message" => "hey there",
                "full_name" => "Oxy Moron",
                "email" => "Oxy@Moron"
            ]
        );
        $this->assertEquals(0, $result);

    }
}
